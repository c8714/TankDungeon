using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
 [SerializeField]
private float _rotationSpeed = 5f;
private float _lastRotation = 0f;
private bool _rotate = false;


private Quaternion _targetRot;
 
private void Awake()
{
    _targetRot = transform.rotation;
}
 
private void Update()
{
   
    Timer();
    Rotater();
}
private void Timer()
{
   
    if(Time.time < _lastRotation +_rotationSpeed) return;
    _lastRotation=Time.time;
    _rotate =true;
    
}
 private void Rotater()
{

    if (_rotate==true)
    { 
        //If you want to rotate each time the mouse is clicked
        _targetRot *= Quaternion.AngleAxis(-90, transform.forward);
    }
    else if (_rotate==true)
    {
        //If you want to rotate each time the mouse is clicked
        _targetRot *= Quaternion.AngleAxis(90, transform.forward);
    }
 
    transform.rotation = Quaternion.Lerp(transform.rotation, _targetRot, _rotationSpeed * Time.deltaTime);
    _rotate=false;
    
}

}
