using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyAttack : MonoBehaviour
{
    [SerializeField]
    private GameObject _fx;
    private void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            Instantiate(_fx,other.gameObject.transform.position,other.gameObject.transform.rotation);
            Destroy(other.gameObject);
            SceneManager.LoadScene("GameOver");

        }

    }
}
