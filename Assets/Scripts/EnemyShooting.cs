using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShooting : MonoBehaviour
{
   
    [SerializeField]
    private Transform _firepoint;
    [SerializeField]
    private LayerMask _wall;
    private bool _shootEnable=true;
    [SerializeField]
    private float _timer = 3f;
    private float _lastTick = 0f;

    // Update is called once per frame
    void FixedUpdate()
    {
       Timer();
       if(_shootEnable==true)
       Shoot();  
        
    }
    void Shoot()
    {
       RaycastHit2D hit = Physics2D.Raycast(_firepoint.position,_firepoint.right,100,_wall);
       if(hit.collider!=null)
       {
       if(hit.collider.CompareTag("Wall"))
       {
           _shootEnable=false;
           Vector2 offsetPoint = Vector2.Lerp(hit.point,transform.position,.25f);
           transform.position=offsetPoint;
       }
       }
    }
    void Timer()
    {
         if(Time.time < _lastTick +_timer) return;
     _lastTick=Time.time;
     _shootEnable =true;
    }
}
