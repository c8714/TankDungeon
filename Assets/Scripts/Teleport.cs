using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour
{
  [SerializeField]
  private GameObject _fx;


  private void OnCollisionEnter2D(Collision2D collision)
  {
      if(collision.gameObject.CompareTag("Wall"))
      {
        Instantiate(_fx,transform.position,transform.rotation);
              
             Destroy(gameObject);
      }
      else
      if(collision.gameObject.CompareTag("Enemy"))
      {
      Instantiate(_fx,transform.position,transform.rotation);
      collision.gameObject.GetComponent<Health>().Life=0;
      Destroy(gameObject);
      }
      if(collision.gameObject.CompareTag("Player"))
      {
        Instantiate(_fx,transform.position,transform.rotation);
       Destroy(gameObject);
      }

  }
    
}
