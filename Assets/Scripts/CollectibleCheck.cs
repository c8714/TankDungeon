using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollectibleCheck : MonoBehaviour
{
 public int Key=0;
 [SerializeField]
 private GameObject _fxPickup;

  private void Start()
 {
     Key=0;
 }
   private void OnTriggerEnter2D(Collider2D other)
   {
       if(other.gameObject.CompareTag("Collectible"))
       {
           Key++;
           Instantiate(_fxPickup,other.gameObject.transform.position,other.gameObject.transform.rotation);
           Destroy(other.gameObject);
           
       }
       if(other.gameObject.CompareTag("Exit"))
       {
           if(Key==3)
           Debug.Log($"Win");
           SceneManager.LoadScene("Win");
       }
   }

}
