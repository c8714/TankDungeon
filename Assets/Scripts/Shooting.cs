using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    [SerializeField]
    private Transform _firepoint;
    [SerializeField]
    private GameObject _projectilePrefab;
    [SerializeField]
    private float _projectileForce;
    [SerializeField]
    private LayerMask _wall;
    private bool _shootEnable=true;
    private float _timer = 1f;
    private float _lastTick = 0f;

    // Update is called once per frame
    void Update()
    {
       Timer();

        if(Input.GetButtonDown("Fire1")&&_shootEnable==true)
        {
            Shoot();
            _shootEnable=false;
        }
        
        
    }
    void Shoot()
    {
    
        GameObject projectile = Instantiate(_projectilePrefab,_firepoint.position,_firepoint.rotation);
       Rigidbody2D rb = projectile.GetComponent<Rigidbody2D>();
       rb.AddForce(_firepoint.right *_projectileForce,ForceMode2D.Impulse);
       RaycastHit2D hit = Physics2D.Raycast(_firepoint.position,_firepoint.right,100,_wall);
       if(hit.collider!=null)
       {
       if(hit.collider.CompareTag("Wall")||hit.collider.CompareTag("Collectible")||hit.collider.CompareTag("Exit"))
       {
           Debug.Log($"Hit");
           Vector2 offsetPoint = Vector2.Lerp(hit.point,transform.position,.25f);
           transform.position=offsetPoint;
       }
       }
    }
    void Timer()
    {
         if(Time.time < _lastTick +_timer) return;
     _lastTick=Time.time;
     _shootEnable =true;
    }

    


}
